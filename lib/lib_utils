#!/usr/bin/env bash

# ##################################################
  # Bash scripting Utilities.
  #
  # VERSION 1.0.0
  #
  # This script sources my collection of scripting utilities making
  # it possible to source this one script and gain access to a
  # complete collection of functions, variables, and other options.
  #
  # HISTORY
  #
  # * 2015-01-02 - v1.0.0  - First Creation
  # * 2016-02-10 - v1.1.1  - Minor changes to satisfy Shellcheck
  # * 2020-12-09 - v1.1.2  - Moved time variables to this file
  #                          Added BASH versioning
  #                          Moved thisHost variable to this file
  #
  # ##################################################

# THISHOST
# ------------------------------------------------------
# Will print the current hostname of the computer the script
# is being run on.
# ------------------------------------------------------
thisHost=$(hostname)

# BASHSYSTEMVERSION
# ------------------------------------------------------
# Version this script is tested on
# ------------------------------------------------------
bashsystemVersion=("5.0.17(1)-release" "4.0.1 l2m1")
currentBashVersion=$BASH_VERSION

# TIMESTAMPS
# ------------------------------------------------------
# Prints the current date and time in a variety of formats:
# ------------------------------------------------------
now=$(LC_ALL=C date +"%m-%d-%Y %r")        				# Returns: 06-14-2015 10:34:40 PM
logtime=$(LC_ALL=C date +"%Y-%m-%d %H:%M:%S")			# Returns: 2015-06-14 20:34:40
datestamp=$(LC_ALL=C date +%Y-%m-%d)       				# Returns: 2015-06-14
hourstamp=$(LC_ALL=C date +%r)             				# Returns: 10:34:40 PM
timestamp=$(LC_ALL=C date +%Y%m%d_%H%M%S)  				# Returns: 20150614_223440
today=$(LC_ALL=C date +"%m-%d-%Y")         				# Returns: 06-14-2015
longdate=$(LC_ALL=C date +"%a, %d %b %Y %H:%M:%S %z")	# Returns: Sun, 10 Jan 2016 20:47:53 -0500
gmtdate=$(LC_ALL=C date -u -R | sed 's/\+0000/GMT/')	# Returns: Wed, 13 Jan 2016 15:55:29 GMT

# Log messages when verbose is set to "true"
function verbose() {
  if [[ "${verbose}" = "true" ]] || [ "${verbose}" == "1" ]; then
    debug "$@"
  fi
}

# File Checks
  # 
  # A series of functions which make checks against the filesystem. For
  # use in if/then statements.
  #
  # Usage:
  #    if is_file "file"; then
  #       ...
  #    fi
function is_exists() {
  if [[ -e "$1" ]]; then
    return 0
  fi
  return 1
}

function is_not_exists() {
  if [[ ! -e "$1" ]]; then
    return 0
  fi
  return 1
}

function is_file() {
  if [[ -f "$1" ]]; then
    return 0
  fi
  return 1
}

function is_not_file() {
  if [[ ! -f "$1" ]]; then
    return 0
  fi
  return 1
}

function is_dir() {
  if [[ -d "$1" ]]; then
    return 0
  fi
  return 1
}

function is_not_dir() {
  if [[ ! -d "$1" ]]; then
    return 0
  fi
  return 1
}

function is_symlink() {
  if [[ -L "$1" ]]; then
    return 0
  fi
  return 1
}

function is_not_symlink() {
  if [[ ! -L "$1" ]]; then
    return 0
  fi
  return 1
}

function is_empty() {
  if [[ -z "$1" ]]; then
    return 0
  fi
  return 1
}

function is_not_empty() {
  if [[ -n "$1" ]]; then
    return 0
  fi
  return 1
}

# Test whether a command exists
  #
  # Usage:
  #    if type_exists 'git'; then
  #      some action
  #    else
  #      some other action
  #    fi
function type_exists() {
  if [ "$(type -P "$1")" ]; then
    return 0
  fi
  return 1
}

function type_not_exists() {
  if [ ! "$(type -P "$1")" ]; then
    return 0
  fi
  return 1
}

# Test which OS the user runs
  #
  # $1 = OS to test
  # Usage: if is_os 'darwin'; then
function is_os() {
  if [[ "${OSTYPE}" == $1* ]]; then
    return 0
  fi
  return 1
}
