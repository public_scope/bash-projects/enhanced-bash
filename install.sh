#!/usr/bin/env bash
# title             :Enhanced BASH Backup File System
# description       :A module to backup a directory or file
# author            :Jessica Brown
# date              :2020-01-25
# usage			    :bakup [--help|-h] [directory/|filename.ext]
# notes			    :This install may not work on all systems, please try to install manually!
# bash_version	    :5.0.17(1)-release, 4.4.19(1)-release
# ==============================================================================
#	1.2.0
#		New bash.conf file being added with questions to fill
#		Modified all path folders to respect installation location versus install from location
#	2.0.0
#		Rewrite of code to contain viable installation system
#	2.3.0
#		Resolved logs folder missing
#		Added pip3 for RHEL versions
#		Separated install dependencies per distro
#		Sanatity checks on each environment
#   git config --global user.email "you@example.com"
#   git config --global user.name "Your Name"
# Setup temp variables to define base locations
version="2.3.0"
scriptLocation="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
White='\e[38;5;15m'
Yellow='\e[38;5;11m'
txtReset='\e[38;5;8m'
Cyan='\e[38;5;51m'
txtReset='\e[0m'
installErrors=0

function compareConfig() {
	printf "# Created by Enhanced BASH Installer on $(LC_ALL=C date +'%Y-%m-%d %H:%M:%S')\n\n# Editor\nexport editor=\"nano\"\n\n# Themes\nexport prompt_theme=\"pureblack\"\n\n# Default Files\nexport logFile=\"startup.log\"\nexport dirListFile=\"directory_list\"\nexport dirLastRemoveFile=\"last_dir_remove\"\n\n# Folders\nexport dirSeparator=\"/\"\nexport binSubPath=\"bin\"\nexport libSubPath=\"lib\"\nexport modSubPath=\"modules\"\nexport logsSubPath=\"logs\"\nexport overrideSubPath=\"overrides\"\nexport themeSubPath=\"themes\"\nexport archiveSubPath=\".backup\"\nexport dirJumpPath=\"dirjump\"\nexport installationSubPath=\"enhanced-bash\"\n\n# History Settings\nexport historyControl=\"ignorespace:erasedups\"\nexport historyAppend=\"histappend\"\nexport historySize=10000\nexport historyFileSize=200000\nexport directoryHistorySize=15\n\n# Keyboard Shortcuts\nexport kbClear=\"\\ec\" # Alt-C clears screen\nexport kbDirJump=\"\\ed\" # Alt-D directory history list\nexport kbReload=\"\\er\" # Alt-R reloads environment\nexport kbVersion=\"\\ev\" # Alt-V show Distro and version information\nexport kbWhoIs=\"\\ew\" # Alt-W show who information\nexport kbHelpFKey=\"\\eOP\" # F1 will display help system\nexport kbHelp=\"\\eh\" # Alternitive (Alt-H) for help if F1 is not available\nexport kbSpashscreenFKey=\"\\eOQ\" # F2 will display the spashscreen\nexport kbSpashscreen=\"\\es\" # Alternitive (Alt-S) for splashscreen if F2 is not available\n\n# Alias Shortcuts\nexport dirjumpCommand=\"d\"\n" > "${scriptLocation}/bin/bash.conf"
	diff "${scriptLocation}/bin/bash.conf" "${scriptLocation}/temp.dat"
	rm "${scriptLocation}/temp.dat"
	bashConfig
}

function bashConfig() {
	# Create the Config file if it doesn't exist
	if [ -f "${scriptLocation}/bin/bash.conf" ]; then
		echo "Existing config file exists, would you like to replace it, use it, compare the new to existing, or backup existing and use new? (Default is backup and use new)"
		read -p "$(echo -e ${White}[${Cyan}R${White}]${txtReset}eplace ${White}[${Cyan}U${White}]${txtReset}se ${White}[${Cyan}C${White}]${txtReset}ompare ${White}[${Cyan}B${White}]${txtReset}ackup ${Yellow}:${txtReset})" configConfim
		if [ $configConfim == "R" ] || [ $configConfim == "r" ]; then
			printf "# Created by Enhanced BASH Installer on $(LC_ALL=C date +'%Y-%m-%d %H:%M:%S')\n\n# Editor\nexport editor=\"nano\"\n\n# Themes\nexport prompt_theme=\"pureblack\"\n\n# Default Files\nexport logFile=\"startup.log\"\nexport dirListFile=\"directory_list\"\nexport dirLastRemoveFile=\"last_dir_remove\"\n\n# Folders\nexport dirSeparator=\"/\"\nexport binSubPath=\"bin\"\nexport libSubPath=\"lib\"\nexport modSubPath=\"modules\"\nexport logsSubPath=\"logs\"\nexport overrideSubPath=\"overrides\"\nexport themeSubPath=\"themes\"\nexport archiveSubPath=\".backup\"\nexport dirJumpPath=\"dirjump\"\nexport installationSubPath=\"enhanced-bash\"\n\n# History Settings\nexport historyControl=\"ignorespace:erasedups\"\nexport historyAppend=\"histappend\"\nexport historySize=10000\nexport historyFileSize=200000\nexport directoryHistorySize=15\n\n# Keyboard Shortcuts\nexport kbClear=\"\\ec\" # Alt-C clears screen\nexport kbDirJump=\"\\ed\" # Alt-D directory history list\nexport kbReload=\"\\er\" # Alt-R reloads environment\nexport kbVersion=\"\\ev\" # Alt-V show Distro and version information\nexport kbWhoIs=\"\\ew\" # Alt-W show who information\nexport kbHelpFKey=\"\\eOP\" # F1 will display help system\nexport kbHelp=\"\\eh\" # Alternitive (Alt-H) for help if F1 is not available\nexport kbSpashscreenFKey=\"\\eOQ\" # F2 will display the spashscreen\nexport kbSpashscreen=\"\\es\" # Alternitive (Alt-S) for splashscreen if F2 is not available\n\n# Alias Shortcuts\nexport dirjumpCommand=\"d\"\n" > "${scriptLocation}/bin/bash.conf"
			source "${scriptLocation}/bin/bash.conf"
		elif [ $configConfim == "U" ] || [ $configConfim == "u" ]; then
			source "${scriptLocation}/bin/bash.conf"
		elif [ $configConfim == "C" ] || [ $configConfim == "c" ]; then
			compareConfig
		elif [ $configConfim == "B" ] || [ $configConfim == "b" ]; then
			mv "${scriptLocation}/bin/bash.conf" "${scriptLocation}/bash.conf.bak"
			printf "# Created by Enhanced BASH Installer on $(LC_ALL=C date +'%Y-%m-%d %H:%M:%S')\n\n# Editor\nexport editor=\"nano\"\n\n# Themes\nexport prompt_theme=\"pureblack\"\n\n# Default Files\nexport logFile=\"startup.log\"\nexport dirListFile=\"directory_list\"\nexport dirLastRemoveFile=\"last_dir_remove\"\n\n# Folders\nexport dirSeparator=\"/\"\nexport binSubPath=\"bin\"\nexport libSubPath=\"lib\"\nexport modSubPath=\"modules\"\nexport logsSubPath=\"logs\"\nexport overrideSubPath=\"overrides\"\nexport themeSubPath=\"themes\"\nexport archiveSubPath=\".backup\"\nexport dirJumpPath=\"dirjump\"\nexport installationSubPath=\"enhanced-bash\"\n\n# History Settings\nexport historyControl=\"ignorespace:erasedups\"\nexport historyAppend=\"histappend\"\nexport historySize=10000\nexport historyFileSize=200000\nexport directoryHistorySize=15\n\n# Keyboard Shortcuts\nexport kbClear=\"\\ec\" # Alt-C clears screen\nexport kbDirJump=\"\\ed\" # Alt-D directory history list\nexport kbReload=\"\\er\" # Alt-R reloads environment\nexport kbVersion=\"\\ev\" # Alt-V show Distro and version information\nexport kbWhoIs=\"\\ew\" # Alt-W show who information\nexport kbHelpFKey=\"\\eOP\" # F1 will display help system\nexport kbHelp=\"\\eh\" # Alternitive (Alt-H) for help if F1 is not available\nexport kbSpashscreenFKey=\"\\eOQ\" # F2 will display the spashscreen\nexport kbSpashscreen=\"\\es\" # Alternitive (Alt-S) for splashscreen if F2 is not available\n\n# Alias Shortcuts\nexport dirjumpCommand=\"d\"\n" > "${scriptLocation}/bin/bash.conf"
			source "${scriptLocation}/bin/bash.conf"
		fi
	else
		printf "# Created by Enhanced BASH Installer on $(LC_ALL=C date +'%Y-%m-%d %H:%M:%S')\n\n# Editor\nexport editor=\"nano\"\n\n# Themes\nexport prompt_theme=\"pureblack\"\n\n# Default Files\nexport logFile=\"startup.log\"\nexport dirListFile=\"directory_list\"\nexport dirLastRemoveFile=\"last_dir_remove\"\n\n# Folders\nexport dirSeparator=\"/\"\nexport binSubPath=\"bin\"\nexport libSubPath=\"lib\"\nexport modSubPath=\"modules\"\nexport logsSubPath=\"logs\"\nexport overrideSubPath=\"overrides\"\nexport themeSubPath=\"themes\"\nexport archiveSubPath=\".backup\"\nexport dirJumpPath=\"dirjump\"\nexport installationSubPath=\"enhanced-bash\"\n\n# History Settings\nexport historyControl=\"ignorespace:erasedups\"\nexport historyAppend=\"histappend\"\nexport historySize=10000\nexport historyFileSize=200000\nexport directoryHistorySize=15\n\n# Keyboard Shortcuts\nexport kbClear=\"\\ec\" # Alt-C clears screen\nexport kbDirJump=\"\\ed\" # Alt-D directory history list\nexport kbReload=\"\\er\" # Alt-R reloads environment\nexport kbVersion=\"\\ev\" # Alt-V show Distro and version information\nexport kbWhoIs=\"\\ew\" # Alt-W show who information\nexport kbHelpFKey=\"\\eOP\" # F1 will display help system\nexport kbHelp=\"\\eh\" # Alternitive (Alt-H) for help if F1 is not available\nexport kbSpashscreenFKey=\"\\eOQ\" # F2 will display the spashscreen\nexport kbSpashscreen=\"\\es\" # Alternitive (Alt-S) for splashscreen if F2 is not available\n\n# Alias Shortcuts\nexport dirjumpCommand=\"d\"\n" > "${scriptLocation}/bin/bash.conf"
		source "${scriptLocation}/bin/bash.conf"
	fi
}

bashConfig
[ -f "${scriptLocation}${dirSeparator}${binSubPath}${dirSeparator}log_system.sh" ] && source "${scriptLocation}${dirSeparator}${binSubPath}${dirSeparator}log_system.sh" || echo "Can not continue"
[ -f "${scriptLocation}${dirSeparator}${libSubPath}${dirSeparator}lib_colors" ] && source "${scriptLocation}${dirSeparator}${libSubPath}${dirSeparator}lib_colors"

# Define default directories
export binInstallLocation="${scriptLocation}${dirSeparator}${binSubPath}"
export libInstallLocation="${scriptLocation}${dirSeparator}${libSubPath}"
export modInstallLocation="${scriptLocation}${dirSeparator}${modSubPath}"
export orInstallLocation="${scriptLocation}${dirSeparator}${overrideSubPath}"
export thmInstallLocation="${scriptLocation}${dirSeparator}${themeSubPath}"
export logsInstallLocation="${binInstallLocation}${dirSeparator}${logsSubPath}"
export archiveInstallLocation="${binInstallLocation}${dirSeparator}${archiveSubPath}"
export backupInstallLocation="${HOME}${dirSeparator}.backup"
export userHomeLocation=$( getent passwd "${USER}" | cut -d: -f6 )
export dirJumpFolder="${binInstallLocation}${dirSeparator}${dirJumpPath}"
export directory_list="${binInstallLocation}${dirSeparator}${dirJumpPath}${dirSeparator}${dirListFile}"
export defaultInstallationLocation="${HOME}${dirSeparator}.local${dirSeparator}share${dirSeparator}applications${dirSeparator}${installationSubPath}"
export defaultSourceLocations=("${libInstallLocation}" "${modInstallLocation}" "${overridesInstallLocation}" "${themesInstallLocation}")
export defaultBackupLocation=("${backupInstallLocation}")
export defaultInstallBaseDirectory="${HOME}${dirSeparator}.local${dirSeparator}share${dirSeparator}applications${dirSeparator}${installationSubPath}"

# Create the log-rotate conf file
if [ ! -d ${dirSeparator}etc${dirSeparator}logrotate.d ]; then
	if [ -f /var/lib/logrotate/logrotate.status ]; then
		sudo mkdir ${dirSeparator}etc${dirSeparator}logrotate.d${dirSeparator}
	else
		echo "Log Rotate is not installed, please install and re-run the installer"
	fi
else
	echo -e "${defaultInstallBaseDirectory}${dirSeparator}${binSubPath}${dirSeparator}${logsSubPath}${dirSeparator}startup.log {\n\tsu $USER $USER\n\tnotifempty\n\tcopytruncate\n\tweekly\n\trotate 52\n\tcompress\n\tmissingok\n}\n" | sudo tee ${dirSeparator}etc${dirSeparator}logrotate.d${dirSeparator}enhanced-bash
fi

# See if they need some help -- Needs to be rewritten
if [[ $1 == "--help" ]]; then
	echo -e "${Red}Notice: ${White}This install script is still under construction, ${Red} DO NOT USE!${txtReset}"
	echo -e "${Silver}There are many things that are not currently completed. Follow these 10 steps to install this program:${txtReset}"
	echo -e "${DeepPink8}Use the ${Silver}--force ${DeepPink8}to force the install and hopfully it won't break your system."
	cursorpos down 2
	cursorpos col 2; echo -e "${Cyan}1. ${Silver}Create the folder: ${Purple}${HOME}/.local/bin${txtReset}"
	cursorpos col 2; echo -e "${Cyan}2. ${Silver}Move the ${Purple}enhanced-bash-script ${Silver}folder to the ${Purple}bin ${Silver}folder.${txtReset}"
	cursorpos col 2; echo -e "${Cyan}3. ${Silver}Rename your .bashrc to .bashrc-org (or whatever you want)${txtReset}"
	cursorpos col 2; echo -e "${Cyan}4. ${Silver}Create a new .bashrc to source the ${Purple}bash_system.sh${txtReset}"
	cursorpos col 2; echo -e "${Cyan}5. ${Silver}Install ${SteelBlue}git, curl, highlight, tput, logrotate${txtReset}"
	cursorpos col 2; echo -e "${Cyan}6. ${Silver}Create the folder: ${Purple}${HOME}/.backup${txtReset}"
	cursorpos col 2; echo -e "${Cyan}7. ${Silver}Create the folder: ${Purple}${HOME}/.local/bin/enhanced-bash-system/lib/dirjump${txtReset}"
	cursorpos col 2; echo -e "${Cyan}8. ${Silver}Create the folder: ${Purple}${defaultBackupLocation}${txtReset}"
	cursorpos col 2; echo -e "${Cyan}9. ${Silver}Create the file: ${Purple}${HOME}/.local/bin/enhanced-bash-system/lib/dirjump/directory_list${txtReset}"
	cursorpos col 2; echo -e "${Cyan}10. ${Silver}Create the file: ${Purple}${HOME}/.local/bin/enhanced-bash-system/lib/dirjump/last_dir_remove${txtReset}"
	cursorpos col 2; echo -e "${Cyan}11. ${Silver}Copy the file: ${Purple}${HOME}/.local/bin/enhanced-bash-system/lib/enhanced-bash to log-rotate folder${txtReset}"
	cursorpos col 2; echo -e "${Cyan}12. ${Silver}Consider contributing from my Git Repo: ${SpringGreen3}https://${SpringGreen3}gitlab.com/${SpringGreen3}public_scope/${SpringGreen3}bash-projects/${DarkSeaGreen7}enhanced-bash-system.git${txtReset}"
	echo -e ${txtReset} && exit
fi

# Create the installation folder (default is $HOME/.local/bin/enhanced-bash/)
read -p "$(echo -e ${White}Enter installation location ${Red}[${SteelBlue2}${defaultInstallationLocation}${Red}]${Aqua}: ${txtReset})" defaultInstallBaseDirectory

if [[ ${defaultInstallBaseDirectory} == "" ]]; then
	export defaultInstallBaseDirectory="${HOME}${dirSeparator}.local${dirSeparator}share${dirSeparator}applications${dirSeparator}${installationSubPath}"
fi

# make the logs folder if it doesn't exist first, we don't want to over write any existing logs and make sure we can write our installation.log file
if [ ! -d "${defaultInstallBaseDirectory}${dirSeparator}${binSubPath}${dirSeparator}${logsSubPath}${dirSeparator}" ]; then
	mkdir -p "${defaultInstallBaseDirectory}${dirSeparator}${binSubPath}${dirSeparator}${logsSubPath}${dirSeparator}" > /dev/null 2>&1
fi 

installLog="${defaultInstallBaseDirectory}${dirSeparator}${binSubPath}${dirSeparator}${logsSubPath}${dirSeparator}install.log"
export installDirectories=("${defaultInstallBaseDirectory}${dirSeparator}${libSubPath}" "${defaultInstallBaseDirectory}${dirSeparator}${modSubPath}" "${defaultInstallBaseDirectory}${dirSeparator}${archiveSubPath}" "${defaultInstallBaseDirectory}${dirSeparator}${dirJumpPath}" "${defaultInstallBaseDirectory}${dirSeparator}${installationSubPath}" "${defaultBackupLocation}")

for installDirectory in ${installDirectories[*]}; do
	if [ ! -d "${installDirectory}" ]; then
		mkdir -p "${installDirectory}" > /dev/null 2>&1
		retVal=$?
		if [ $retVal -ne 0 ]; then
    		error "[Creating directory ${installDirectory}]" >> ${installLog}
			installErrors++
		else
			success "[Creating directory ${installDirectory}]" >> ${installLog}
		fi
	else
		success "[Directory ${installDirectory} already exists]" >> ${installLog}
	fi
done

# make the overrides folder if it doesn't exist, we don't want to over write any existing overrides
if [ ! -d "${defaultInstallBaseDirectory}${dirSeparator}${overridesInstallLocation}" ]; then
	success "[Creating directory ${defaultInstallBaseDirectory}${dirSeparator}${overridesInstallLocation}]" >> ${installLog}
	mkdir -p "${defaultInstallBaseDirectory}${dirSeparator}${overridesInstallLocation}" > /dev/null 2>&1
else
	success "[Directory ${defaultInstallBaseDirectory}${dirSeparator}${overridesInstallLocation} already exists]" >> ${installLog}
fi 

# Check and install some fonts for dependancies
[ ! -d "${userHomeLocation}${dirSeparator}.local${dirSeparator}share${dirSeparator}fonts${dirSeparator}" ] && mkdir -p "${userHomeLocation}${dirSeparator}.local${dirSeparator}share${dirSeparator}fonts${dirSeparator}"
if [ ! -f "${userHomeLocation}${dirSeparator}.local${dirSeparator}share${dirSeparator}fonts${dirSeparator}PowerlineSymbols.otf" ]; then
	wget https://github.com/powerline/powerline/raw/develop/font/PowerlineSymbols.otf
	mv PowerlineSymbols.otf "${userHomeLocation}${dirSeparator}.local${dirSeparator}share${dirSeparator}fonts${dirSeparator}"
fi

[ ! -d "${userHomeLocation}${dirSeparator}.config${dirSeparator}fontconfig${dirSeparator}conf.d${dirSeparator}" ] && mkdir -p "${userHomeLocation}${dirSeparator}.config${dirSeparator}fontconfig${dirSeparator}conf.d${dirSeparator}"	
if [ ! -f "${userHomeLocation}${dirSeparator}.config${dirSeparator}fontconfig${dirSeparator}conf.d${dirSeparator}10-powerline-symbols.conf" ]; then
	wget https://github.com/powerline/powerline/raw/develop/font/10-powerline-symbols.conf
	mv 10-powerline-symbols.conf "${userHomeLocation}${dirSeparator}.config${dirSeparator}fontconfig${dirSeparator}conf.d${dirSeparator}"
fi

# Install the minimal dependancies - jq git curl highlight most wget python pip
# TODO: Check to see if any exist and only install those that don't
packagesNeededNull='jq git curl highlight most wget python3 python3-pip'
packagesNeededDebian='jq git curl highlight most wget python3 python3-pip fontconfig'
packagesNeededRHEL='jq git curl highlight wget python3 python3-pip https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm'
echo -e "${Red}NOTICE: ${Silver}This script will install ${Yellow}${packagesNeededNull}${Silver} sudo will be requested, please enter your password.${txtReset}"

if [ -x "$(command -v apk)" ]; then
	success "[APK package manager detected]" >> ${installLog}
	sudo apk add --no-cache $packagesNeededDebian -y

	# Add command logging
	echo -e "local6.*                ${defaultInstallBaseDirectory}${dirSeparator}${binSubPath}${dirSeparator}${logsSubPath}/commandhistory.log" | sudo tee ${dirSeparator}etc${dirSeparator}rsyslog.conf
	sudo service rsyslog restart

	# Add the fonts to cache
	sudo fc-cache -vf ~/.local/share/fonts/
	# TODO: Check if powerline-status exists
	pip3 install --user powerline-status
elif [ -x "$(command -v apt-get)" ]; then
	success "[APT-GET package manager detected]" >> ${installLog}
	sudo apt-get install $packagesNeededDebian -y

	# Add command logging
	echo -e "local6.*                ${defaultInstallBaseDirectory}${dirSeparator}${binSubPath}${dirSeparator}${logsSubPath}/commandhistory.log" | sudo tee ${dirSeparator}etc${dirSeparator}rsyslog.conf
	sudo service rsyslog restart

	# Add the fonts to cache
	sudo fc-cache -vf ~/.local/share/fonts/
	# TODO: Check if powerline-status exists
	pip install --user powerline-status
elif [ -x "$(command -v dnf)" ]; then
	success "[DNF package manager detected]" >> ${installLog}
	sudo dnf install $packagesNeededRHEL -y && sudo dnf update -y

	# Add command logging
	echo -e "local6.*                ${defaultInstallBaseDirectory}${dirSeparator}${binSubPath}${dirSeparator}${logsSubPath}/commandhistory.log" | sudo tee ${dirSeparator}etc${dirSeparator}syslog.conf
	sudo service syslog restart

	# Add the fonts to cache
	sudo fc-cache -vf ~/.local/share/fonts/
	# TODO: Check if powerline-status exists
	pip3 install --user powerline-status
elif [ -x "$(command -v zypper)" ]; then
	success "[ZYPPER package manager detected]" >> ${installLog}
	sudo zypper install $packagesNeededDebian -y

	# Add command logging
	echo -e "local6.*                ${defaultInstallBaseDirectory}${dirSeparator}${binSubPath}${dirSeparator}${logsSubPath}/commandhistory.log" | sudo tee ${dirSeparator}etc${dirSeparator}rsyslog.conf
	sudo service rsyslog restart

	# Add the fonts to cache
	sudo fc-cache -vf ~/.local/share/fonts/
	# TODO: Check if powerline-status exists
	pip3 install --user powerline-status
elif [ -x "$(command -v pkg)" ]; then
	success "[PKG package manager detected]" >> ${installLog}
	sudo pkg install $packagesNeededRHEL -y

	# Add command logging
	echo -e "local6.*                ${defaultInstallBaseDirectory}${dirSeparator}${binSubPath}${dirSeparator}${logsSubPath}/commandhistory.log" | sudo tee ${dirSeparator}etc${dirSeparator}rsyslog.conf
	sudo service rsyslog restart

	# Add the fonts to cache
	sudo fc-cache -vf ~/.local/share/fonts/
	# TODO: Check if powerline-status exists
	pip3 install --user powerline-status
else 
	error "[package manager not detected]" >> ${installLog}
	installErrors++
	installErrorMessages="\nCould not find a package manager"
	echo "FAILED TO INSTALL PACKAGE: Package manager not found. You must manually install: $packagesNeededNull">&2;
fi

# Copy all of the files to the new folder and remove the install.sh script
cp -r ${scriptLocation}${dirSeparator}* ${defaultInstallBaseDirectory}${dirSeparator}
success "[installation folder found at ${defaultInstallBaseDirectory}${dirSeparator}]" >> ${logsInstallLocation}${dirSeparator}install.log
rm ${defaultInstallBaseDirectory}${dirSeparator}install.sh

# Rename the current .bashrc to .bashrc-$timestamp-EBS
mv "${HOME}/.bashrc" "${HOME}/.bashrc-$(LC_ALL=C date +%Y%m%d_%H%M%S)-EBS"
# Create the new .bashrc to source to the enhanced-bash-system.sh
printf "# Created by Enhanced BASH Installer on $(LC_ALL=C date +'%Y-%m-%d %H:%M:%S')\n\ncase \"\$TERM\" in\n\txterm-color|screen|*-256color)\n\t\tcd ${defaultInstallBaseDirectory}${dirSeparator}\n\t\t. ${defaultInstallBaseDirectory}${dirSeparator}/bash_system.sh;;\nesac\n" > ~/.bashrc

# Create the new directory jump folder and files
mkdir -p ${dirJumpFolder}
touch ${dirJumpFolder}${dirSeparator}directory_list

# TODO: Make a Success or Error banner with a pitiful self promotion link to the gitlab page.
if [ ${installErrors} == 0 ]; then
	success "[Enhanced BASH system has been installed successfully]" >> ${installLog}
else
	error "[Enhanced BASH system had an error: ${installErrorMessages}]" >> ${installLog}
fi