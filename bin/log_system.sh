function _alert() {
  if [ "${1}" = "emergency" ]; then local color="\e[38;5;9m"; fi
  if [ "${1}" = "error" ]; then local color="\e[38;5;9m"; fi
  if [ "${1}" = "warning" ]; then local color="\e[38;5;214m"; fi
  if [ "${1}" = "notice" ]; then local color="\e[38;5;51m"; fi
  if [ "${1}" = "info" ]; then local color="\e[38;5;67m"; fi
  if [ "${1}" = "success" ]; then local color="\e[38;5;2m"; fi
  if [ "${1}" = "debug" ]; then local color="\e[38;5;129m"; fi
  if [ "${1}" = "header" ]; then local color="\e[38;5;180m"; fi
  if [ "${1}" = "input" ]; then local color=""; export printLog="false"; fi
  
  # Don't use colors on pipes or non-recognized terminals
  if [[ "${TERM}" != "xterm"* ]] || [ -t 1 ]; then local color=""; export reset=""; fi

  # Print to console when script is not 'quiet'
  if [[ "${quiet}" = "true" ]] || [ "${quiet}" == "1" ]; then
    return
  else
    echo -e "\e[38;5;117m$(date +"%D")\e[38;5;156m$(date +" %r")\e[38;5;99m$(date +" %Z")\e[38;5;67m$(date +" %:z") ${color}$(printf "[%9s]" "${1}") \e[0m${_message}${reset}"
  fi

}

function die () {
  _message="${*} Exiting.";
  _alert emergency
  safeExit;
}

function error () {
  _message="${*}";
  _alert error
}

function warning () {
  _message="${*}";
  _alert warning
}

function notice () {
  _message="${*}";
  _alert notice
}

function info () {
  _message="${*}";
  _alert info
}

function debug () {
  _message="${*}";
  _alert debug
}

function success () {
  _message="${*}";
  _alert success
}

function input() {
  _message="${*}";
  _alert input;
}

function header() {
  _message="========== ${*} ==========  ";
  _alert header
}

function verbose() {
  if [[ "${verbose}" = "true" ]] || [ "${verbose}" == "1" ]; then
    debug "$@"
  fi
}

function is_exists() {
  if [[ -e "$1" ]]; then
    return 0
  fi
  return 1
}

function is_not_exists() {
  if [[ ! -e "$1" ]]; then
    return 0
  fi
  return 1
}

function is_file() {
  if [[ -f "$1" ]]; then
    return 0
  fi
  return 1
}

function is_not_file() {
  if [[ ! -f "$1" ]]; then
    return 0
  fi
  return 1
}

function is_dir() {
  if [[ -d "$1" ]]; then
    return 0
  fi
  return 1
}

function is_not_dir() {
  if [[ ! -d "$1" ]]; then
    return 0
  fi
  return 1
}

function is_symlink() {
  if [[ -L "$1" ]]; then
    return 0
  fi
  return 1
}

function is_not_symlink() {
  if [[ ! -L "$1" ]]; then
    return 0
  fi
  return 1
}

function is_empty() {
  if [[ -z "$1" ]]; then
    return 0
  fi
  return 1
}

function is_not_empty() {
  if [[ -n "$1" ]]; then
    return 0
  fi
  return 1
}

function type_exists() {
  if [ "$(type -P "$1")" ]; then
    return 0
  fi
  return 1
}

function type_not_exists() {
  if [ ! "$(type -P "$1")" ]; then
    return 0
  fi
  return 1
}

function is_os() {
  if [[ "${OSTYPE}" == $1* ]]; then
    return 0
  fi
  return 1
}